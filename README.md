# 语雀SDK For .NetCore

#### 介绍
适用于.NetCore的语雀Sdk，支持Async/await模型，返回类型与语雀官方保持一致。

适用于.NetCore3.1及以上SDK。不支持.NetFramework


#### 安装

- nuget中搜索YuQueSdk安装即可。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0908/144245_495565f0_1801048.png "屏幕截图.png")

- 或者在nuget命令中执行
``` 
Install-Package YuQueSdk
```

#### 使用说明

- 引入命名空间

``` csharp
using YuQueSdk;
```

- 创建连接对象

``` csharp
YuQueClient yuQueClient = new YuQueClient("您的用户token");
```
用户token可以在 [语雀个人设置](https://www.yuque.com/settings/tokens) 中获取到

- 获取一个知识库的目录

``` csharp
YuQueClient yuQueClient = new YuQueClient("您的用户token");
List<Topic> topics = yuQueClient.GetRepoTopic("docRepo");
```

- 获取一个知识库的目录树形结构

``` csharp
YuQueClient yuQueClient = new YuQueClient("您的用户token");
List<TopicTree> topics = yuQueClient.GetRepoTopicTree("docRepo");
```

`docRepo`为文档所属的知识库 如：https://www.yuque.com/yuque/developer/repo 中的【yuque/developer】

- 获取文档的详情

``` csharp
YuQueClient yuQueClient = new YuQueClient("您的用户token");
DocDetail docDetail= yuQueClient.GetDocDetail("docRepo", "slug");
```

`docRepo`为文档所属的知识库 如：https://www.yuque.com/yuque/developer/repo 中的【yuque/developer】

`slug`为文档的路径 如：https://www.yuque.com/yuque/developer/repo 中的【repo】


> 更多API正在编写中，如有问题欢迎提问。
